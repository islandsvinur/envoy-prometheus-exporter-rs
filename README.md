# Enphase Envoy Prometheus exporter

This is a Prometheus exporter for [Enphase IQ Gateways](https://enphase.com/store/communication/iq-gateway) -- also
called [Envoy or IQ Envoy](https://support.enphase.com/s/article/What-is-the-Envoy) -- and possibly other gateways by
Enphase as well.

## Building

1. Install Rust: `curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh`
2. Install build dependencies: `apt install libssl-dev` (or similar)
3. Store an **access token** in a file `token.txt`. See the documentation
   for [accessing IQ Gateway local APIs or local UI with token-based authentication](https://enphase.com/download/iq-gateway-access-using-local-apis-or-local-ui-token-based-authentication-tech-brief).
4. Run: `cargo run` or build for release `cargo build --release`
