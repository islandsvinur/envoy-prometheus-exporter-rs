use std::convert::Infallible;
use std::fmt::format;
use std::fs;
use std::net::SocketAddr;
use std::fs::File;
use std::io::Read;
use std::time::Duration;
use clap::Parser;
use http_body_util::Full;
use hyper::body::Bytes;
use hyper::server::conn::http1;
use hyper::service::service_fn;
use hyper_util::rt::TokioIo;
use log::info;

use prometheus::{Counter, Encoder, Gauge, GaugeVec, IntCounter, IntCounterVec, IntGauge, IntGaugeVec, register_counter, register_gauge, register_gauge_vec, register_int_counter, register_int_counter_vec, register_int_gauge, register_int_gauge_vec, TextEncoder};
use reqwest::{Certificate, Client};
use serde::Deserialize;
use tokio::net::TcpListener;
use tokio::time;


// Initialize a counter metric
lazy_static::lazy_static! {
    static ref INVERTERS_ACTIVE_COUNT: IntGauge = register_int_gauge!(
        "envoy_inverters_active_count",
        "The number of active inverters"
    ).unwrap();
    static ref INVERTERS_WATTS_NOW: IntGauge = register_int_gauge!(
        "envoy_inverters_watts_now",
        "The current power of all inverters combined"
    ).unwrap();
    static ref INVERTERS_WATTHOURS_LIFETIME: IntCounter = register_int_counter!(
        "envoy_inverters_watthours_lifetime",
        "The total energy produced by all inverters over the whole lifetime of the installation"
    ).unwrap();
    static ref INVERTER_LATEST_REPORT_TIME: IntCounterVec = register_int_counter_vec!(
        "envoy_inverter_latest_report_time",
        "The last time an inverter reported their production",
        &["inverter"],
    ).unwrap();
    static ref INVERTER_LATEST_REPORT_WATT: IntGaugeVec = register_int_gauge_vec!(
        "envoy_inverter_latest_report_watt",
        "The latest reported produced power of an inverter",
        &["inverter"],
    ).unwrap();
    static ref INVERTER_MAX_REPORT_WATT: IntGaugeVec = register_int_gauge_vec!(
        "envoy_inverter_max_report_watt",
        "The maximum reported produced power of an inverter",
        &["inverter"],
    ).unwrap();
}

#[derive(Parser, Debug)]
#[command(version, about, long_about = None)]
struct Args {
    /// Base URL of the Envoy gateway
    #[arg(short, long, default_value_t = (& "https://envoy.local").to_string())]
    base_url: String,

    /// File containing access token to use for Envoy gateway
    #[arg(short, long, default_value_t = (& "token.txt").to_string())]
    token_file: String,

    /// Port the Prometheus exporter should listen on
    #[arg(short, long, default_value_t = 9898)]
    port: u16,
}

#[derive(Deserialize, Debug)]
struct Production {
    production: Box<[Inverter]>,
}


#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Inverter {
    active_count: i64,
    reading_time: i64,
    w_now: i64,
    wh_lifetime: i64,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct InverterDetail {
    serial_number: String,
    last_report_date: i64,
    dev_type: i64,
    last_report_watts: i64,
    max_report_watts: i64,
}

async fn create_client() -> Result<Client, Box<dyn std::error::Error>> {
    // Create a reqwest client that accepts the self-signed certificate
    let client = Client::builder()
        // rusttls does not support the self-signed CA certificates of the Envoy
        .danger_accept_invalid_certs(true)
        .timeout(Duration::from_secs(10))
        .build()?;

    return Ok(client);
}

async fn fetch_and_update_metric(base_url: String, token: String) {
    let client = create_client().await.unwrap();

    loop {
        match client.get(format!("{}/production.json", base_url))
            .header("Accept", "application/json")
            .header("Authorization", format!("Bearer {}", token))
            .send()
            .await {
            Ok(response) => {
                let bytes = response.bytes().await;
                if let Ok(body) = bytes {
                    if let Ok(json) = serde_json::from_slice::<Production>(&body) {
                        for inverter in json.production.iter() {
                            INVERTERS_ACTIVE_COUNT.set(inverter.active_count as i64);
                            INVERTERS_WATTS_NOW.set(inverter.w_now as i64);
                            let prev_watthours_lifetime = INVERTERS_WATTHOURS_LIFETIME.get();
                            INVERTERS_WATTHOURS_LIFETIME.inc_by(inverter.wh_lifetime as u64 - prev_watthours_lifetime);
                            // eprintln!("Active count: {}, Now W:, {} Lifetime Wh: {}", inverter.active_count, inverter.w_now, inverter.wh_lifetime);
                        }
                    }
                }
            }
            Err(e) => {
                eprintln!("Error fetching JSON: {e:?}");
            }
        }

        match client.get(format!("{}/api/v1/production/inverters", base_url))
            .header("Accept", "application/json")
            .header("Authorization", format!("Bearer {}", token))
            .send()
            .await {
            Ok(response) => {
                let bytes = response.bytes().await;
                if let Ok(body) = bytes {
                    if let Ok(json) = serde_json::from_slice::<Vec<InverterDetail>>(&body) {
                        for inverter in json.iter() {
                            let prev_time = INVERTER_LATEST_REPORT_TIME.get_metric_with_label_values(&[&inverter.serial_number]).unwrap().get();
                            INVERTER_LATEST_REPORT_TIME.with_label_values(&[&inverter.serial_number]).inc_by(inverter.last_report_date as u64 - prev_time);
                            INVERTER_LATEST_REPORT_WATT.with_label_values(&[&inverter.serial_number]).set(inverter.last_report_watts as i64);
                            INVERTER_MAX_REPORT_WATT.with_label_values(&[&inverter.serial_number]).set(inverter.max_report_watts as i64);
                            // eprintln!("Now: {}, Serial:, {} Last W: {}, Max W: {}", inverter.last_report_date, inverter.serial_number, inverter.last_report_watts, inverter.max_report_watts);
                        }
                    }
                }
            }
            Err(e) => {
                eprintln!("Error fetching JSON: {e:?}");
            }
        }

        time::sleep(Duration::from_secs(5)).await; // Fetch every 60 seconds
    }
}

async fn serve_metrics(_: hyper::Request<hyper::body::Incoming>) -> Result<hyper::Response<Full<Bytes>>, Infallible> {
    let encoder = TextEncoder::new();
    let metric_families = prometheus::gather();
    let mut buffer = Vec::new();
    encoder.encode(&metric_families, &mut buffer).unwrap();

    let response = hyper::Response::builder()
        .header("Content-Type", encoder.format_type())
        .body(Full::new(Bytes::from(buffer)))
        .unwrap();

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let args: Args = Args::parse();

    env_logger::init();

    let token: String = fs::read_to_string(args.token_file)?;
    tokio::spawn(fetch_and_update_metric(args.base_url, token));

    // Define the address and create the server
    let addr = SocketAddr::from(([127, 0, 0, 1], args.port));
    info!("listening on {addr}");

    // We create a TcpListener and bind it to 127.0.0.1:3000
    let listener = TcpListener::bind(addr).await?;

    // We start a loop to continuously accept incoming connections
    loop {
        let (stream, _) = listener.accept().await?;

        // Use an adapter to access something implementing `tokio::io` traits as if they implement
        // `hyper::rt` IO traits.
        let io = TokioIo::new(stream);

        // Spawn a tokio task to serve multiple connections concurrently
        tokio::task::spawn(async move {
            // Finally, we bind the incoming connection to our `hello` service
            if let Err(err) = http1::Builder::new()
                // `service_fn` converts our function in a `Service`
                .serve_connection(io, service_fn(serve_metrics))
                .await
            {
                eprintln!("Error serving connection: {:?}", err);
            }
        });
    }
}
